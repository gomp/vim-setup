#!/bin/bash
mkdir -p ~/.vim
mkdir -p ~/.vim/colors
wget https://raw.github.com/jnurmine/Zenburn/master/colors/zenburn.vim -P ~/.vim/colors/
cat > ~/.vimrc << EOF
" disable old Vi compatibility mode
set nocompatible

" indentation
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab

" color scheme and highlighting
syntax on
set number
set ruler
set t_Co=256
set background=dark
colorscheme zenburn
let g:zenburn_force_dark_Background=1
EOF
